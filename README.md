# Leading High Performing Teams

Jesteś managerem, team leaderem (formalnym bądź nieformalnym) pracującym z zespołem programistów.

Twoja firma przeszła transformację na zwinne wytwarzanie oprogramowania, masz Scruma w jednym palcu, zespół pisze czysty kod, a w firmie wszyscy powtarzają, że autonomia, mistrzostwo i cel to klucz do motywacji pracowników.

To szkolenie idzie krok dalej, po to aby wspólnie zastanowić się co oznacza autonomia, mistrzostwo i cel na głębszym poziomie, w kontekście pracy programistów oraz czego w tym modelu brakuje. Bazując na case studies, badaniach naukowych, inspiracjach z branży i spoza niej, analizujemy czym w praktyce są "ludzie i interakcje" w wysokowydajnych zespołach.

# W trakcie szkolenia nauczysz się:

* Traktować przyjmowanie nowych kolegów i koleżanek do pracy jako pełnowartościową pracę pierwszej kategorii, a nie przykry obowiązek.
* Zapewniać swoim współpracownikom poczucie bezpieczeństwa w codziennej pracy i częściej zauważać gdy go brakuje.
* Wspierać budowanie umiejętności eksperckich wśród swoich współpracowników i rozpoznawać co może blokować szybsze postępy.
* Rozpoznawać dlaczego niektórym osobom trudno razem pracować i jak dostosować swój przekaz do różnych poziomów zaawansowania.
* Minimalizować wpływ błędów poznawczych przy trudnych wyborach i podejmować odrobinę lepsze decyzje technologiczne czy rekrutacyjne.
* Podnosić odpowiedzialność i zaangażowanie zespołu w realizację celów bez komenderowania i kontroli.
* Projektować środowisko pracy pod kątem responsywności a nie optymalizacji kosztowej.
* Kwestionować mainstreamowe praktyki w swoim kontekście.

# Agenda

## Agile development: coś poszło nie tak
## Bezpieczeństwo
### nawyk kluczowy
### przykłady naruszenia bezpieczeństwa pracownika IT
### teatr bezpieczeństwa
### kultura blameless
### doświadczenie pierwszego dnia
### chartering
### "psychological safety" i bezpieczne spotkania
### charyzma uwagi
## Nauka
### nastawienie na rozwój i nastawienie na trwałość
### Kanban zaaplikowany do nauki
### "pożądane trudności" kluczem do efektywnej nauki
### "celowe ćwiczenia" kluczem do budowanie umiejętności eksperckich
### harmonogram twórcy i głęboka praca
### nowicjusze, eksperci i "najlepsze praktyki"
### Dreyfus Squared: konfiguracje pracy w parach
## Decyzje
### błędy poznawcze w inżynierii oprogramowania
### systematyczne radzenie sobie z błędami - framework WRAP
### poszerz wachlarz możliwości (np. multitracking)
### skonfrontuj założenia z rzeczywistością (np. adwokat diabła)
### nabierz dystansu (np. reguła 10/10/10)
### przygotuj się na błędy (np. premortem)
## Cele
### nadrzędny cel inżynierii oprogramowania
### produkty zamiast projektów (#noprojects)
### mityczne "business value" oraz impakt biznesowy
### "move fast and don't break things" - Continuous Delivery wersja matematyczna
### co warto mierzyć, a czego nie warto mierzyć
### zespół zorientowany na wynik, a nie na aktywność oraz mit zajętości
### utrzymać wysoką motywację: reguła postępu, szympansy i pszczoły